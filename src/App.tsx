import { PageLayout } from "./Components/PageLayout/PageLayout";
import Pages from "./Pages/pages";


export default function App() {

  return (
    <PageLayout>
        <Pages />
    </PageLayout>
  );
}
