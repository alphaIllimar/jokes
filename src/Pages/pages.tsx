import { Routes, Route } from "react-router-dom";
import { Jokes } from "./Jokes/Jokes";
import { Saved } from "./Saved/Saved";

export const JOKES_URL:string = '/';
export const SAVED_URL:string = '/saved';

export default function Pages() 
{
    return (
        <Routes>
            <Route path={JOKES_URL} element={<Jokes />} />
            <Route path={SAVED_URL} element={<Saved />} />
        </Routes>
    );
}