import React from "react";
import { IState, useState } from "../../React-Hooks/useState";
import { AppService } from "../../Services/AppService/AppService";
import { CollapsedJokes } from "../../Components/CollapsedJokes/CollapsedJokes";

export function Jokes(){

    const _categories:IState<string[]> = useState<string[]>([]);

    React.useEffect(() => {
        AppService.getCategories().then(categories => {
            _categories.set(categories);
        });
    }, []);

    return (
        <>
        <CollapsedJokes />
        {
            _categories.value.map((category:string) => (
                <CollapsedJokes category={category} key={category} />
            ))
        }
        </>
        
    );
}