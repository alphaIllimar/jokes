import { Joke } from "../../Components/Joke/Joke";
import { IJoke } from "../../Models/IJoke";
import { AppSaving } from "../../Services/AppService/AppService";

export function Saved() {
    
    const _allSavedJokes:IJoke[] = AppSaving.all();

    return (
        <>
            {
                _allSavedJokes.map((joke) => (
                    <Joke joke={joke} key={joke.id} />
                ))
            }
        </>
    );
}