export interface IJoke {
    /**
     * The unique ID of the joke
     */
    id:string;
    /**
     * The actual joke as a string
     */
    value:string;
}