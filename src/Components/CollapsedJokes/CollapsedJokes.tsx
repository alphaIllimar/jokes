import { ExpandMore } from "@mui/icons-material";
import { Accordion, AccordionDetails, AccordionSummary, Typography } from "@mui/material";
import { ICollapsedJokesProps } from "./ICollapsedJokesProps";
import { IState, useState } from "../../React-Hooks/useState";
import { IJoke } from "../../Models/IJoke";
import { AppService } from "../../Services/AppService/AppService";
import { Joke } from "../Joke/Joke";


export function CollapsedJokes(props:ICollapsedJokesProps) {

    const _jokes:IState<IJoke[]> = useState<IJoke[]>([]);

    return (
        <Accordion
            expanded={!!_jokes.value.length}
        >
            <AccordionSummary
                expandIcon={<ExpandMore />}
                onClick={() => {
                    if(_jokes.value.length) {
                        _jokes.set([]);
                    } else {
                        AppService.getJokes(props.category).then((jokes:IJoke[]) => {
                            _jokes.set(jokes);
                        });
                    }
                }}
            >
                <Typography variant="body1">{ props.category || 'ranom' }</Typography>
            </AccordionSummary>
            <AccordionDetails>
                {
                    _jokes.value.map((joke:IJoke) => (
                        <Joke joke={joke} key={joke.id} />
                    ))
                }
            </AccordionDetails>
        </Accordion> 
    )
}