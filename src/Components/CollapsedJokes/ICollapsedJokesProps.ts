export interface ICollapsedJokesProps {
    /**
     * If set shows what category for these jokes are for otherwise will show random jokes
     */
    category?:string
}