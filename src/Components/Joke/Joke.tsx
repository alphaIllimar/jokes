import { Card, CardContent, IconButton, Stack, Typography } from "@mui/material";
import { IJokeProps } from "./IJokeProps";
import { Favorite } from "@mui/icons-material";
import { AppSaving } from "../../Services/AppService/AppService";
import { IState, useState } from "../../React-Hooks/useState";

export function Joke(props:IJokeProps) {

    const _isSaved:IState<boolean> = useState<boolean>(AppSaving.isSaved(props.joke));

    return (
        <Card
            sx={{
                marginBottom: '0.25em'
            }}
        >
            <CardContent>
                <Stack 
                    direction={'row'} 
                    spacing={1}
                >
                    <Typography 
                        variant="body1"
                        sx={{
                            flexGrow: 1
                        }}
                    >
                        { props.joke.value }
                    </Typography>
                    <IconButton 
                        sx={{
                            cursor: 'pointer'
                        }}
                        onClick={() => {
                            if(_isSaved.value) {
                                AppSaving.delete(props.joke);
                            }
                            else {
                                AppSaving.save(props.joke);
                            }

                            _isSaved.set(!_isSaved.value);
                        }}
                    >
                        <Favorite color={_isSaved.value ? 'error' : 'disabled'} />
                    </IconButton>
                </Stack>                
            </CardContent>
        </Card>
    )
}