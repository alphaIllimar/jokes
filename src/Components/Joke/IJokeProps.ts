import { IJoke } from "../../Models/IJoke";

export interface IJokeProps {
    /**
     * The joke to be displayed
     */
    joke:IJoke;
}