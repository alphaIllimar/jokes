import { Tab, Tabs } from '@mui/material';
import * as React from 'react';

import { JOKES_URL, SAVED_URL } from '../../Pages/pages';
import { useLocation, useNavigate } from 'react-router-dom';

export function AppTabs() {
    const _navigate = useNavigate();
    const _handleChange = (e:React.SyntheticEvent, newPath:string) => {
        _navigate(newPath);
    };

    return (
        <Tabs value={useLocation().pathname.toLowerCase()} onChange={_handleChange} >
            <Tab 
                label="Jokes" 
                value={JOKES_URL}

            />
            <Tab 
                label="Saved" 
                value={SAVED_URL}
            />
        </Tabs>
    )
}