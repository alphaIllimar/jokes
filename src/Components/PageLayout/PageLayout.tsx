import * as React from 'react';
import { AppTabs } from '../AppTabs/AppTabs';
import { Box } from '@mui/material';

export function PageLayout(props:React.PropsWithChildren<{}>) {
    return (
        <div>
            <AppTabs />
            <Box
                sx={{
                    paddingTop: '0.5em'
                }}
            >
                {props.children}
            </Box>
        </div>
    );
}