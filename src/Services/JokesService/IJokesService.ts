import { IJoke } from "../../Models/IJoke";

/**
 * Describes the jokes services that gets the jokes from the source
 */
export interface IJokesService
{
    /**
     * Returns a promise with all the joke categories as a string of category names
     */
    getCategories:() => Promise<string[]>;
    /**
     * Gets 3 jokes of the given category
     * @param category If ret will get jokes from this category if not then gets random jokes
     */
    getJokes:(category?:string) => Promise<IJoke[]>;
}