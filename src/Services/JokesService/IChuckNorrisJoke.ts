import { IJoke } from "../../Models/IJoke";

export interface IChuckNorrisJoke extends IJoke {
    created_at:string;
    icon_url:string;
    updated_at:string;
    url:string;
}