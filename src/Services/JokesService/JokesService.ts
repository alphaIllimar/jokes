import { Format } from "../../Helpers/Format";
import { IJoke } from "../../Models/IJoke";
import { IChuckNorrisJoke } from "./IChuckNorrisJoke";
import { IJokesService } from "./IJokesService";
import * as _ from 'lodash';

/**
 * The joke API endpoint for the categories
 */
const CATEGORY_ENDPOINT_URL:string = 'https://api.chucknorris.io/jokes/categories';

const RANDOM_JOKE_ENDPOINT:string = 'https://api.chucknorris.io/jokes/random';
const CATEGORY_JOKE_ENDPOINT:string = 'https://api.chucknorris.io/jokes/random?category={0}';


type GetNorrisJoke = Promise<IChuckNorrisJoke|null>;

export class JokesService implements IJokesService
{
    /**
     * Returns the response object from the fetch result
     * @param res The fetch result
     */
    private async _getResponse<T>(res: Response): Promise<T> {
        const responseText: string = await res.text();

        //parses the request
        try {
            const response: any = JSON.parse(responseText);

            //for error
            if (!res.ok) {
                throw response;
            }
            //for success
            else {
                return response;
            }
        } catch (e) {
            throw `Could not parse respone: ${responseText}`;
        }
    }
    
    public async getCategories():Promise<string[]> {
        let ret:string[] = [];

        const reqOptions = {
            method: 'get',
            headers: new Headers(),
            body: null,
        };

        const fetchResult: Response = await fetch(CATEGORY_ENDPOINT_URL, reqOptions);

        try {
            ret = await this._getResponse(fetchResult);
        } catch(e) {
        }

        return ret;
    }
    /**
     * Returns a random joke from the API
     */
    private async _getRandomJoke():Promise<IChuckNorrisJoke|null> {
        let ret:IChuckNorrisJoke|null = null;

        const reqOptions = {
            method: 'get',
            headers: new Headers(),
            body: null,
        };

        const fetchResult: Response = await fetch(RANDOM_JOKE_ENDPOINT, reqOptions);

        try {
            ret = await this._getResponse<IChuckNorrisJoke>(fetchResult) as IChuckNorrisJoke; 
        } catch(e) {
        }

        return ret;
    }

    /**
     * Returns a category joke from the API
     * @param category The category id (name) of the category whom joke you seek
     */
    private async _getCategoryJoke(category:string):Promise<IChuckNorrisJoke|null> {
        let ret:IChuckNorrisJoke|null = null;

        const reqOptions = {
            method: 'get',
            headers: new Headers(),
            body: null,
        };

        const fetchResult: Response = await fetch(Format.text(CATEGORY_JOKE_ENDPOINT, category), reqOptions);

        try {
            ret = await this._getResponse<IChuckNorrisJoke>(fetchResult) as IChuckNorrisJoke; 
        } catch(e) {
        }

        return ret;
    }

    public async getJokes(category?:string):Promise<IJoke[]> {

        const getJoke:() => GetNorrisJoke = category ? (
            () => this._getCategoryJoke(category)
        ) : () => this._getRandomJoke()

        const promiseArray:GetNorrisJoke[] = [];
        promiseArray.push(getJoke());
        promiseArray.push(getJoke());
        promiseArray.push(getJoke());
        
        let jokes:(IChuckNorrisJoke|null)[] = await Promise.all(promiseArray);
        jokes = _.uniqBy(jokes, o => o?.id);

        //makes sure there are no nulls in the jokes
        let index = jokes.length;
        while(index--) {
            if(!jokes[index]) {
                jokes.splice(index, 1);
            }
        }

        return jokes as IChuckNorrisJoke[];
    }
}