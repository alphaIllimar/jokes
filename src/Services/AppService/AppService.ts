import { IJokesService } from "../JokesService/IJokesService";
import { JokesService } from "../JokesService/JokesService";
import { ISaveService } from "../SaveService/ISaveService";
import { SaveService } from "../SaveService/SaveService";

export const AppService:IJokesService = new JokesService();
export const AppSaving:ISaveService = new SaveService();