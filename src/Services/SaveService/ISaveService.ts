import { IJoke } from "../../Models/IJoke";

export interface ISaveService {
    /**
     * Saves the joke to the storage
     * @param joke The joke to be saved
     */
    save:(joke:IJoke) => boolean;
    /**
     * Returns all the jokes saved
     */
    all:() => IJoke[];
    /**
     * Allows you to check if a joke has been added to storage or not
     * @param joke The joke to be checked
     */
    isSaved(joke:IJoke):boolean;
    /**
     * Deletes a joke from storage
     * @param joke The joke to be deleted from storage
     */
    delete:(joke:IJoke) => void;
}