import { IJoke } from "../../Models/IJoke";
import { ISaveService } from "./ISaveService";

const LOCAL_STORAGE_KEY:string = 'saved-jokes';

export class SaveService implements ISaveService
{
    public all():IJoke[] {
        const allJokes:IJoke[] = JSON.parse(localStorage.getItem(LOCAL_STORAGE_KEY) || '[]');
        return allJokes;
    }
    public save(joke:IJoke) {
        try {
            const allJokes:IJoke[] = this.all();
            allJokes.push(joke);
            localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(allJokes));
            return true;
        }
        catch(e) {
            console.error('SAVE-ERROR', e);
            return false;
        }
    }
    public isSaved(joke:IJoke):boolean {
        return !!this.all().find((j:IJoke) => j.id === joke.id);
    }
    public delete(joke:IJoke) {
        const allJokes = this.all().filter((j:IJoke) => j.id !== joke.id);
        localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(allJokes));
    }
}